<?php

class CsvDb {

    private $filename;
    private $database = [];
    private $columnNames = [];
    private $columnIdx = [];
    private $columns = 0;
    private $rows = 0;
    private $dbExists = false;

    public function __construct($filename)
    {
        $this->filename = $filename;
        if (file_exists($this->filename)) {
            $this->openDb();
        } else {
            $fp = fopen($this->filename, 'w');
            fclose($fp);
        }
    }

    private function openDb()
    {
        if ($this->filename == "") {
            echo "No database file set\n";
            return;
        }

        if (!($data = file($this->filename))) {
            echo "Cannot open database file\n";
            return;
        }

        $this->columnNames = str_getcsv($data[0]);

        $db = [];
        $fileRows = count($data);
        for ($row = 1; $row < $fileRows; $row++) {
            $db[] = str_getcsv($data[$row]);
        }

        $this->database = $db;
        $this->setMetaData();
        $this->dbExists = true;
    }

    private function setMetaData()
    {
        foreach ($this->columnNames as $k => $v) {
            $this->columnIdx[$v] = $k;
        }

        $this->columns = count($this->columnNames);

        $this->rows = count($this->database);
    }

    /**
     * Writes all data from database array to db file
     * @return bool True on success, false on failure
     */
    private function syncDb()
    {
        $dbFile = fopen($this->filename, "w");
        flock($dbFile, LOCK_EX);

        fputcsv($dbFile, $this->columnNames);
        foreach ($this->database as $row) {
            fputcsv($dbFile, $row);
        }

        flock($dbFile, LOCK_UN);
        fclose($dbFile);

        return true;
    }

    public function dbExists()
    {
        return $this->dbExists;
    }

    public function setSchema($schema = [])
    {
        $this->columnNames = $schema;
        $this->setMetaData();

        $this->syncDb();
    }

    /**
     * Select record where $column == $value
     * @param  string     $column Column name to query
     *                            * = return all rows (does not require $value to be set)
     * @param  string|int $value  Column value to query
     * @return array              Array of found rows
     */
    public function select($column, $value = null)
    {
        if ($column != '*') {
            if (!in_array($column, $this->columnNames)) {
                throw new Exception("Invalid column name: " . $column);
            }
            $idx = $this->columnIdx[$column];
        }
        $resultSet = [];
        $cnt = 0;
        foreach ($this->database as $key => $row) {
            if ($column == '*' || $row[$idx] == $value) {
                $tmpRow = [];
                $tmpRow['row_id'] = $key;
                // reset keys to known column names
                foreach ($row as $i => $v) {
                    $tmpRow[$this->columnNames[$i]] = $v;
                }
                $resultSet[] = $tmpRow;
            }
        }
        return $resultSet;
    }

    public function selectCount($column, $value)
    {
        if (!in_array($column, $this->columnNames)) {
            throw new Exception("Invalid column name: " . $column);
        }

        $idx = $this->columnIdx[$column];
        $count = 0;
        foreach ($this->database as $key => $row) {
            if ($row[$idx] == $value) {
                $count++;
            }
        }

        return $count;
    }

    public function insert($record)
    {
        $dbFile = fopen($this->filename, "a");
        flock($dbFile, LOCK_EX);
        if (fputcsv($dbFile, $record)) {
            $this->database[] = $record;
            $this->rows++;
        }
        flock($dbFile, LOCK_UN);
        fclose($dbFile);

        return true;
    }

    /**
     * Update a single row by rowId
     *   First, run a select to get the correct row_id,
     *   then run update with that row_id
     * @param  int    $rowId  Database row id
     * @param  string $column Column name to change
     * @param  string $data   New data
     * @return bool
     */
    public function update($rowId, $column, $data)
    {
        if ($rowId < 0 || $rowId >= $this->rows) {
            return false;
        }
        if (!in_array($column, $this->columnNames)) {
            return false;
        }

        $columnIdx = $this->columnIdx[$column];

        foreach ($this->database as $k => $v) {
            if ($k == $rowId) {
                $this->database[$k][$columnIdx] = $data;
                $this->syncDb();
                break;
            }
        }
        return true;
    }

    public function delete($rowId)
    {
        if ($rowId < 0 || $rowId >= $this->rows) {
            return false;
        }

        unset($this->database[$rowId]);
        $this->syncDb();

        return true;
    }

    /**
     * Getter for database row count.
     * @return integer
     */
    public function rows() {
        return $this->rows;
    }

    /**
     * Getter for database column count.
     * @return integer
     */
    public function columns() {
        return $this->columns;
    }

    /**
     * Getter for database column names.
     * @return array
     */
    public function columnNames($column = null) {
        if ($column !== null) {
            return $this->columnNames[$column];
        }
        // else return all column names
        return implode(',', $this->columnNames);
    }
}


// EXAMPLE USAGE

$csvDbColor = new CsvDb(dirname(__FILE__) . '/database.csv');
$csvDbColor->setSchema(['color', 'rank']);
$data[] = ['red', 1];
$data[] = ['green', 6];
$data[] = ['blue', 7];
$data[] = ['orange', 9];
$data[] = ['brown', 7];
$data[] = ['black', 6];
$data[] = ['gray', 7];
foreach ($data as $k => $v) {
    $csvDbColor->insert($v);
}
$results = $csvDbColor->select('rank', 7);
echo "csvDbColor database:\n";
print_r($results);

if (!$csvDbColor->delete(9)) {
    echo "could not delete idx 9\n";
}

$result = $csvDbColor->select('color', 'black');
if (count($result) > 0) {
    $csvDbColor->delete($result[0]['row_id']);
}

$results = $csvDbColor->select('*');
echo "csvDbColor records:\n";
print_r($results);

$startTime = microtime(true);
$csvDbBig = new CsvDb(dirname(__FILE__) . '/db_big.csv');
if (!$csvDbBig->dbExists()) {
    $csvDbBig->setSchema(['id', 'number']);
    $rows = [];
    for ($i = 0; $i < 10000; $i++) {
        $num = rand(1000, 9999);
        $csvDbBig->insert([$i, $num]);
    }
}

$results = $csvDbBig->select('number', 8030);
echo "Record info for value 8030:\n";
print_r($results);

$endTime = microtime(true);
printf("Benchmark time: %0.3f seconds\n", $endTime - $startTime);
